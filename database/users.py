from flask import Flask, render_template, request, redirect
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
# app = Flask(__name__)
# # tell flask where database will be stored
# app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///lotus.db'
# db = SQLAlchemy(app)


class Users(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    email = db.Column(db.Text, nullable=False)
    password_hashed = db.Column(db.Text, nullable=False)
    date_created = db.Column(
        db.DateTime, nullable=False, default=datetime.utcnow)
    public_id = db.Column(db.Text, nullable=False, default=email)
    username = db.Column(db.Text, nullable=False, default=email)
    role = db.Column(db.Text, nullable=False, default="user")
    print("User Table Created")

    def __repr__(self):  # will re-print whenever we have a new entry in db
        return '<User xcxc {}>' + str(self.email)

    def payload(self):
        return {
            "id": self.id,
            "email": self.email,
            "password_hashed": self.password_hashed,
            "date_created": self.date_created,
            "public_id": self.public_id,
            "username": self.username,
            "role": self.role
        }

# if __name__ == "__main__":
#     app.run(debug=True)  # give error info instead of only giving 404
