import random
import json
import gym
from gym import spaces
import pandas as pd
import numpy as np

MAX_ACCOUNT_BALANCE = 2147483647
MAX_NUM_SHARES = 2147483647
MAX_SHARE_PRICE = 5000
MAX_OPEN_POSITIONS = 5
MAX_STEPS = 20000

INITIAL_ACCOUNT_BALANCE = 10000


class StockTradingEnvs(gym.Env):
    """A stock trading environment for OpenAI gym"""
    metadata = {'render.modes': ['human']}

    def __init__(self, stock_pool, initial_balance=50000, max_balance=5000000, commission=0.1):
        super(StockTradingEnvs, self).__init__()
        self.holdings = []
        self.stock_pool = stock_pool
        self.initial_balance = initial_balance
        self.balance = initial_balance
        self.max_balance = max_balance
        self.commission = commission
        self.current_day = '2017-01-02'
        self.current_timeStep = '09:15:00+05:30'
        self.transaction_history = []
        self.shares_held = 0
        # Actions of the format Buy x%, Sell x%, Hold, etc.
        self.action_space = spaces.Box(
            low=np.array([0, 0]), high=np.array([3, 1]), dtype=np.float16)

        # Prices contains the OHCL values for the last five prices
        self.observation_space = spaces.Box(
            low=0, high=1, shape=(6, 5), dtype=np.float16)

    def _next_observation(self):
        # Get the stock data points for the last 5 days and scale to between 0-1
        # drive.mount('/content/drive')
        df = pd.read_csv(
            '/content/drive/My Drive/projects/Artificial Intelligence/Database/TCS__EQ__NSE__NSE__MINUTE.csv')
        df = df.sort_values('Date')
        frame = np.array([
            self.df.loc[self.current_step: self.current_step +
                        5, 'Open'].values / MAX_SHARE_PRICE,
            self.df.loc[self.current_step: self.current_step +
                        5, 'High'].values / MAX_SHARE_PRICE,
            self.df.loc[self.current_step: self.current_step +
                        5, 'Low'].values / MAX_SHARE_PRICE,
            self.df.loc[self.current_step: self.current_step +
                        5, 'Close'].values / MAX_SHARE_PRICE,
            self.df.loc[self.current_step: self.current_step +
                        5, 'Volume'].values / MAX_NUM_SHARES,
        ])

        # Append additional data and scale each value to between 0-1
        obs = np.append(frame, [[
            self.balance / MAX_ACCOUNT_BALANCE,
            self.max_net_worth / MAX_ACCOUNT_BALANCE,
            self.shares_held / MAX_NUM_SHARES,
            self.cost_basis / MAX_SHARE_PRICE,
            self.total_shares_sold / MAX_NUM_SHARES,
            self.total_sales_value / (MAX_NUM_SHARES * MAX_SHARE_PRICE),
        ]], axis=0)

        return obs

    def fetchCurentPrice(self):
        # return current price for the stock
        pass

    def _take_action(self, action):

        # Set the current price to a random price within the time step
        current_price = self.df.loc[(
            self.current_date, self.current_time), "current_price"]
        # random.uniform(
        #     self.df.loc[self.current_step, "Open"], self.df.loc[self.current_step, "Close"])
        #stock = action[0]
        action_type = action[0]
        amount = action[1]
        print(f'action_type {action[1]} amount {action[2]}')

        if action_type < 1:
            # Buy amount % of balance in shares
            current_price = self.fetchCurrentPrice()
            total_possible = int(self.balance / current_price)
            shares_bought = int(total_possible * amount)
            transaction_cost = shares_bought * current_price

            self.balance -= transaction_cost
            self.average_price = (
                (self.shares_held*self.average_price) + transaction_cost) / (self.shares_held + shares_bought)
            self.shares_held += shares_bought
            self.reward = 0

            for share in range shares_bought:
                holdings.append(current_price)
            self.transaction_history.append({
                'transation_time': (self.current_date, self.current_time),
                'balance': self.balance,
                'shares_held': self.shares_held,
                'average_price': self.average_price,
                'action_type': 'Buy',
                'action_code': action_type,
                'amount': shares_bought,
                'buy_price': current_price,
                'sell_price': "",
                'profit_percent': 0

            })

        elif action_type < 2:
            # Sell amount % of shares held

            current_price = self.fetchCurrentPrice()
            shares_sold = int(self.shares_held * amount)
            amount = shares_sold * current_price
            #commission_cost = (amount * self.commission) / 100
            self.balance += amount
            self.shares_held -= shares_sold
            cost_price = shares_sold * self.average_price
            selling_price = amount
            self.reward = ((sell_price - cost_price) * 100) / cost_price
            self.transaction_history.append({
                'transation_time': (self.current_date, self.current_time),
                'balance': self.balance,
                'shares_held': self.shares_held,
                'average_price': self.average_price,
                'action_type': 'Sell',
                'action_code': action_type,
                'amount': shares_sold,
                'buy_price': "",
                'sell_price': current_price,
                'profit_percent': 0
            })

        elif action_type < 3:
            # Hold amount % of shares held
            self.reward = 0
            self.transaction_history.append({
                'transation_time': (self.current_date, self.current_time),

                'balance': self.balance,
                'shares_held': self.shares_held,
                'action_type': 'Hold',
                'action_code': action_type,
                'amount': "",
                'buy_price': "",
                'sell_price': "",

            })

        #self.net_worth = self.balance + self.shares_held * current_price

        # if self.net_worth > self.max_net_worth:
        #     self.max_net_worth = self.net_worth

    def step(self, action):
        # Execute one time step within the environment
        self._take_action(action)

        #self.current_step += 1
        # increment time

        # done = True if day end else done = False

        # if self.current_step > len(self.df.loc[:, 'Open'].values) - 6:
        #     self.current_step = 0

        #delay_modifier = (self.current_step / MAX_STEPS)

        #reward = self.balance - self.initial_balance

        # reward = profit percentage

        obs = self._next_observation()

        return obs, reward, done, {}

    def reset(self):
        # Reset the state of the environment to an initial state
        self.balance = INITIAL_ACCOUNT_BALANCE
        self.net_worth = INITIAL_ACCOUNT_BALANCE
        self.max_net_worth = INITIAL_ACCOUNT_BALANCE
        self.shares_held = 0
        self.cost_basis = 0
        self.total_shares_sold = 0
        self.total_sales_value = 0

        # Set the current step to a random point within the data frame
        self.current_step = random.randint(
            0, len(self.df.loc[:, 'Open'].values) - 6)

        return self._next_observation()

    def render(self, mode='human', close=False):
        # Render the environment to the screen
        profit = self.net_worth - INITIAL_ACCOUNT_BALANCE

        print(f'Step: {self.current_step}')
        print(f'Balance: {self.balance}')
        print(
            f'Shares held: {self.shares_held} (Total sold: {self.total_shares_sold})')
        print(
            f'Avg cost for held shares: {self.cost_basis} (Total sales value: {self.total_sales_value})')
        print(
            f'Net worth: {self.net_worth} (Max net worth: {self.max_net_worth})')
        print(f'Profit: {profit}')
