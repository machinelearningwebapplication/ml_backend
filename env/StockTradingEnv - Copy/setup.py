from setuptools import setup

setup(name='StockTradingEnvs-v0',
      version='0.0.1',
      install_requires=['gym>=0.2.3',
                        'pandas>=0.1',
                        'numpy>=0.1']
      )
