from gym.envs.registration import register

register(
    id='StockTradingEnvs-v0',
    entry_point='StockTradingEnvs.envs:StockTradingEnvs',
)
