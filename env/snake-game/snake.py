import pygame
import time
import random
import numpy as np
pygame.init()

white = (255, 255, 255)
yellow = (255, 255, 102)
black = (0, 0, 0)
red = (213, 50, 80)
green = (0, 255, 0)
blue = (50, 153, 213)

dis_width = 100
dis_height = 100

dis = pygame.display.set_mode((dis_width, dis_height))
pygame.display.set_caption('Snake Game Basic')

clock = pygame.time.Clock()

snake_block = 10
snake_speed = 1

font_style = pygame.font.SysFont("bahnschrift", 25)
score_font = pygame.font.SysFont("comicsansms", 35)


def printState(snakeArray, food):
    state = np.zeros(
        (int(dis_height/10), int(dis_width/10)), dtype=int)
    for node in snakeArray:
        state[int(node[1]/10)][int(node[0]/10)] = 1
    state[int(food[1]/10)][int(food[0]/10)] = 3
    print(state)

    # #state[snakeArray[-1]/10][snakeArray[-1]/10] = 2
    # state[int(food[0]/10)][int(food[1]/10)] = 3
    # for x in range(int(dis_height/10)):
    #     print("\n")
    #     for y in range(int(dis_width/10)):
    #         print(state[x])


def Your_score(score):
    value = score_font.render("Your Score: " + str(score), True, yellow)
    dis.blit(value, [0, 0])


def message(msg, color):
    mesg = font_style.render(msg, True, color)
    dis.blit(mesg, [dis_width / 6, dis_height / 3])


def gameLoop():
    game_over = False
    game_close = False

    x1 = dis_width / 2
    y1 = dis_height / 2

    x1_change = 0
    y1_change = 0

    snake_List = []
    Length_of_snake = 1

    foodx = round(random.randrange(0, dis_width - snake_block) / 10.0) * 10.0
    foody = round(random.randrange(0, dis_height - snake_block) / 10.0) * 10.0

    while not game_over:

        while game_close == True:
            dis.fill(blue)
            message("C or Q", red)
            #Your_score(Length_of_snake - 1)
            pygame.display.update()

            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_q:
                        game_over = True
                        game_close = False
                    if event.key == pygame.K_c:
                        gameLoop()
                if event.type == pygame.QUIT:
                    game_over = True
                    game_close = False

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                game_over = True
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    x1_change = -snake_block
                    y1_change = 0
                elif event.key == pygame.K_RIGHT:
                    x1_change = snake_block
                    y1_change = 0
                elif event.key == pygame.K_UP:
                    y1_change = -snake_block
                    x1_change = 0
                elif event.key == pygame.K_DOWN:
                    y1_change = snake_block
                    x1_change = 0

        x1 += x1_change
        y1 += y1_change
        if x1 >= dis_width:
            x1 = 0
        if x1 < 0:
            x1 = dis_width - snake_block
        if y1 >= dis_height:
            y1 = 0
        if y1 < 0:
            y1 = dis_height - snake_block

        dis.fill(blue)
        pygame.draw.rect(dis, green, [foodx, foody, snake_block, snake_block])
        snake_Head = []
        snake_Head.append(x1)
        snake_Head.append(y1)
        snake_List.append(snake_Head)
        if len(snake_List) > Length_of_snake:
            del snake_List[0]

        for x in snake_List[:-1]:
            if x == snake_Head:
                game_close = True

        for x in snake_List:
            pygame.draw.rect(
                dis, black, [x[0], x[1], snake_block, snake_block])
            pygame.draw.rect(
                dis, white, [snake_Head[0], snake_Head[1], snake_block, snake_block])

        #Your_score(Length_of_snake - 1)

        pygame.display.update()

        if x1 == foodx and y1 == foody:
            foodx = round(random.randrange(
                0, dis_width - snake_block) / 10.0) * 10.0
            foody = round(random.randrange(
                0, dis_height - snake_block) / 10.0) * 10.0
            Length_of_snake += 1

        printState(snake_List, [foodx, foody])

        clock.tick(snake_speed)

    pygame.quit()
    quit()


gameLoop()
