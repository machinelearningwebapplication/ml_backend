import numpy as np
import random

# Environment Variables
maze_width = 7
maze_height = 7
x = random.randint(0, maze_height-2)
y = random.randint(0, maze_width-2)
food_x = 6
food_y = 6
# Hyperparameters
exploration_rate = 0.5
discount = 0.5
# Initial Declaration


class Maze():
    def __init__(self):
        self.x = random.randint(0, maze_height-2)
        self.y = random.randint(0, maze_width-2)
        self.action_space = [4]
        self.state_space = np.zeros((maze_height, maze_width), dtype=int)
        self.state_space[food_x][food_y] = 3
        self.state_space[self.x][self.y] = 1
        self.reward = 0

    def moveRover(self, action):
        self.state_space[self.x][self.y] = 0
        if action == 0:
            print("Left")
            if self.y - 1 < 0:
                self.y = 0
            else:
                self.y += -1
        elif action == 1:  # Right
            print("Right")
            if self.y + 1 >= maze_width:
                self.y = maze_width - 1
            else:
                self.y += 1
        elif action == 2:  # Up
            print("Up")
            if self.x - 1 < 0:
                self.x = 0
            else:
                self.x += -1
        elif action == 3:  # Down
            print("Down")
            if self.x + 1 >= maze_height:
                self.x = maze_height - 1
            else:
                self.x += 1

        self.state_space[self.x][self.y] = 1


# Testing
model = Maze()
print(model.state_space)
print(model.action_space[0])
for i in range(10):
    print(i)
    action = random.randint(0, model.action_space[0])
    model.moveRover(action)
    print(model.state_space)
