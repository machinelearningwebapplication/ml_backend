from flask import Flask, render_template, request, redirect, jsonify, make_response
from flask_sqlalchemy import SQLAlchemy
import datetime


# Database Specific Imports
import jwt
from functools import wraps
# Generate Hash password Imports
from werkzeug.security import generate_password_hash, check_password_hash
import uuid
# from database.users import Users, db


app = Flask(__name__)
# tell flask where database will be stored
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///ml.db"
# secrect key to encrypt token
app.config["SECRET_KEY"] = "thisissecret"

db = SQLAlchemy(app)


class Users(db.Model):
    id = db.Column(db.BigInteger, primary_key=True,
                   default=lambda: uuid.uuid4().hex)
    email = db.Column(db.Text, nullable=False, unique=True)
    password_hashed = db.Column(db.Text, nullable=False)
    date_created = db.Column(
        db.DateTime, nullable=False, default=datetime.datetime.utcnow)
    username = db.Column(db.Text, nullable=False)
    role = db.Column(db.Text, nullable=False, default="user")

    def __repr__(self):  # will re-print whenever we have a new entry in db
        return '<User xcxc {}>' + str(self.email)

    def payload(self):
        return {
            "id": self.id,
            "email": self.email,
            "password_hashed": self.password_hashed,
            "date_created": self.date_created,
            "username": self.username,
            "role": self.role
        }
    # def __repr__(self):  # will re-print whenever we have a new entry in db
    #     return "<User {}>" + str(self.email)


# I've never used SQLAlchemy before but you should visit the SQLAlchemy official website they will provide you with coding patterns to avoid/better-handle these kind of things
def recreateDatabase():  # Funtion TO Delete Entire Previous Database and Create New One

    # Clear Database
    db.drop_all()
    print("Database Deleted")
    # Initiate Db
    db.create_all()
    print("Database Initiated")


def token_required(f):
    # this is the best approach to making sure that token is present! Good work!! :)
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None

        if "x-access-token" in request.headers:
            token = request.headers["x-access-token"]

        if not token:
            return make_response({"message": "Token is missing!"}, 401, {"WWW-Authenticate": "Basic realm='Login required!'"})

        try:
            data = jwt.decode(token, app.config["SECRET_KEY"])
            current_user = Users.query.filter_by(email=data["email"]).first()
        except:
            return make_response({"message": "Token is invalid!"}, 401)

        return f(current_user, *args, **kwargs)

    return decorated

# Fetch details for one user using id or all users
# Consider this principle "In a controller/Route handler function should never have more than 15 lines of code, if it increases more then 15 lines, then you're probable doing more than what Route is intended to do."
# so basically your route should only do the Following
#  * Cleanse user parameters/input
#  * Store/retreive data in database
#  * Check Permissions(you can make a generic function that checks user permission)
@app.route("/user/<string:id>", methods=['GET'])
@token_required
def printUserTable(current_user, id):
    try:

        if id == "-1" or id == "all":  # check if all users requested or only 1
            # never do this, create separate routes, for generic all users and to specifically getting 1 single user.
            # use route plural names such as users, orders, etc. this makes understanding API very easy and when designing APIs consider the following point.
            # API will only be created once but will be used by multiple clients so make sure that APIs a designed from the Client's prespective. There are many companies that boosted their business only because they provided a simpler/easier API design instead of all in one purpose API
            user_all = Users.query.order_by(
                Users.id, Users.date_created).all()  # Show all users
        else:
            user_all = Users.query.filter_by(
                id=id).first()  # Show user whose id match
        # for user in all_users:
        if not user_all:  # Check if any users present
            return make_response({"message": "No users found!"}, 401)
        output = []
        for u in user_all:
            output.append(u.payload())
            # Improvement
        return jsonify({"users": output})
        # return user json

    except Exception as e:
        return make_response({"Error Fetching Users"}, 401)

# Add new user
@app.route("/register", methods=["POST"])
def create_user():
    data = request.get_json()
    try:
        if not "email" in data:  # check if email is present in request
            return make_response({"message": "Email Required"}, 400)
        check_email = Users.query.filter_by(email=data["email"]).first()
        if check_email is not None:
            return make_response({"message": "Email Already In Use"}, 400)
        email = data["email"]
        if not "password" in data:
            return make_response({"message": "Password Required"}, 400)
        password_hashed = generate_password_hash(
            data["password"], method="sha256")  # hash and store user password
        if not "username" in data:  # If no username is provided assign email as default username
            username = data["email"]
        else:
            username = data["username"]
        if not "role" in data:
            role = None
        else:
            # Check if no role provided then assign none so that database will assign default value for role (i.e. "user"])
            role = data["role"]

        # A the above steps can be reduced by simply creating a function that dynamically checks for every parameter present with type.
        new_user = Users(email=email, password_hashed=password_hashed,
                         username=username, role=role)
        db.session.add(new_user)  # add new user to users table
        db.session.commit()  # save database

        return make_response({"message": "New user created! "}, 200)
    except Exception as e:
        return make_response({"message": "Unable to add new user"}, 400)

# Login to the system and get jwt token & id
@app.route('/login', methods=["GET"])
def login():
    try:
        auth = request.authorization
        if not auth or not auth.username or not auth.password:
            return make_response('Could not verify', 401, {'WWW-Authenticate': 'Basic realm="Login required!"'})

        user = Users.query.filter_by(email=auth.username).first()

        if user is None:
            return make_response('Incorrect Credentials', 401, {'WWW-Authenticate': 'Basic realm="Login required!"'})

        if check_password_hash(user.password_hashed, auth.password):
            token = jwt.encode({'email': user.email, 'exp': datetime.datetime.utcnow(
            ) + datetime.timedelta(minutes=30)}, app.config['SECRET_KEY'])

            return make_response({'token': token.decode('UTF-8'), 'id': user.id}, 200)

        return make_response('Incorrect Credentials', 401, {'WWW-Authenticate': 'Basic realm="Login required!"'})
    except Exception as e:
        return make_response({"message": "Unable to login", "Exception": e}, 400)


# Main Function Calls
recreateDatabase()  # comment once db has been initialised to prevent loss of data


if __name__ == "__main__":
    app.run(debug=True)  # give error info instead of only giving 404

# each class beaces a data for database


# class BlogPosts(mydb.Model):
#     id = mydb.Column(mydb.Integer, primary_key=True)
#     title = mydb.Column(mydb.String(100), nullable=False)
#     content = mydb.Column(mydb.Text, nullable=False)
#     author = mydb.Column(mydb.String(20), nullable=False, default="N/A")
#     date_posted = mydb.Column(
#         mydb.DateTime, nullable=False, default=datetime.utcnow)

#     def __repr__(self):  # will re-print whenever we have a new entry in db
#         return "Blog Post " + str(self.id)


# all_posts = [
#     {
#         "title": "Post 1",
#         "content": "This is post 1 content",
#         "author": "xyz"
#     },
#     {
#         "title": "Post 2",
#         "content": "This is post 2 content"
#     }
# ]
# url path if for a live website it would be @app.route("www.mywebsitedomain.com/")
# @app.route("/")
# def base():
#     return render_template("index.html")


# @app.route("/posts", methods=["GET", "POST"])
# def post():
#     if request.method == "POST":
#         post_title = request.form["title"]  # fetch data from html form
#         post_content = request.form["content"]  # fetch data from html form
#         if(request.form["author"] != ""):
#             post_author = request.form["author"]  # fetch data from html form
#         else:
#             post_author = None
#         # Insert new row in db
#         new_post = BlogPosts(
#             title=post_title, content=post_content, author=post_author)
#         mydb.session.add(new_post)  # only save for run time
#         mydb.session.commit()  # commit permanently
#         return redirect("/posts")  # refresh page to reload data
#     else:
#         all_posts = BlogPosts.query.order_by(BlogPosts.date_posted).all()
#         return render_template("posts.html", posts=all_posts)


# @app.route("/posts/delete/<int:id>")
# def postDelete(id):
#     post = BlogPosts.query.get_or_404(id)
#     mydb.session.delete(post)
#     mydb.session.commit()
#     return redirect("/posts")


# @app.route("/posts/edit/<int:id>", methods=["GET", "POST"])
# def postEdit(id):
#     if request.method == "POST":
#         post = BlogPosts.query.get_or_404(id)
#         post.title = request.form["title"]  # fetch data from html form
#         post.content = request.form["content"]  # fetch data from html form
#         if(request.form["author"] != ""):
#             post.author = request.form["author"]  # fetch data from html form
#         else:
#             post.author = "N/A"
#         mydb.session.commit()
#         return redirect("/posts")
#     if request.method == "GET":
#         post = BlogPosts.query.get_or_404(id)
#         return render_template("edit.html", post=post)
# # @app.route("/home")
# # def home():
# #     return "You can enter name in url"

# # Data Type
# # String
# # id
# @app.route("/home<string:var1>")
# def home(var1):
#     return "Welcome "+var1


# @app.route("/onlygetpost", methods=["GET", "POST"])
# def only_get_post():
#     return "Only Get and Post Method allowed for this page."
